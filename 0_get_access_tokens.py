from rauth.service import OAuth1Service, OAuth1Session

# Get a real consumer key & secret from: https://www.goodreads.com/api/keys
API_KEY    = 'PGy7Ocp5Y6pTBT9pecbwQ'
API_SECRET = 'UMBQrspr1dfdsWE4FOU8e1tmRJ0nWswep5pKPgXk8'

goodreads = OAuth1Service(
	consumer_key    = API_KEY,
	consumer_secret = API_SECRET,
	name='goodreads',
	request_token_url = 'https://www.goodreads.com/oauth/request_token',
	authorize_url     = 'https://www.goodreads.com/oauth/authorize',
	access_token_url  = 'https://www.goodreads.com/oauth/access_token',
	base_url          = 'https://www.goodreads.com/'
)

OAUTH_TOKEN, OAUTH_SECRET = goodreads.get_request_token(header_auth=True)
authorize_url = goodreads.get_authorize_url(OAUTH_TOKEN)
print(authorize_url)
# Visit the above URL then return and run the following
session = goodreads.get_auth_session(OAUTH_TOKEN, OAUTH_SECRET)
ACCESS_TOKEN = session.access_token
ACCESS_TOKEN_SECRET = session.access_token_secret

print("ACCESS_TOKEN        = \"" + ACCESS_TOKEN + "\"")
print("ACCESS_TOKEN_SECRET = \"" + ACCESS_TOKEN_SECRET + "\"")

