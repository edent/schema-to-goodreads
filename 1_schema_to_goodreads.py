from rauth.service import OAuth1Service, OAuth1Session
import requests
from bs4 import BeautifulSoup
import json
import re
import xml.etree.ElementTree as ET
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("URl")
args = parser.parse_args()
print(args.URl)

#	URL containing a JSON+LD script

review_url = args.URl

#	Get the URL

r = requests.get(review_url)

#	Find the review Schema

soup = BeautifulSoup(r.text, features="html.parser")
pattern = re.compile(r"\"@type\":\"Review\"")
script = soup.find("script", text=pattern)
review = script.text
review_json = json.loads(review)

#	Get the details from the review

isbn        = review_json["itemReviewed"]["isbn"]
rating      = review_json["reviewRating"]["ratingValue"]
date        = review_json["datePublished"]
description = review_json["description"] + " " + review_url

#	Get a real consumer key & secret from: https://www.goodreads.com/api/keys

API_KEY    = 'PGy7Ocp5Y6pTBT9pecbwQ'
API_SECRET = 'UMBQrspr1dfdsWE4FOU8e1tmRJ0nWswep5pKPgXk8'

#	Get the tokens from 0_get_access_tokens.py

ACCESS_TOKEN        = "nuudG1Q5PHQEJQMSNXlA"
ACCESS_TOKEN_SECRET = "TeaeZcc1FommmjbNw4oW0ZjYFxnDkv88sdbhpSNIlE"

#	Convert the ISBN to a Goodreads ID

goodreads_id = requests.get("https://www.goodreads.com/book/isbn_to_id/" + isbn + "?key=" + API_KEY).text

#	Construct the review

review_data = {"book_id": goodreads_id, "review[review]":description, "review[rating]":rating, "review[read_at]":date[0:10]}

print(isbn)
print(review_data)
#	Connect to the API

goodreads_api = OAuth1Session(
	consumer_key        = API_KEY,
	consumer_secret     = API_SECRET,
	access_token        = ACCESS_TOKEN,
	access_token_secret = ACCESS_TOKEN_SECRET,
)

#	Post the review to the API
#	https://www.goodreads.com/api/index#review.create

response = goodreads_api.post('https://www.goodreads.com/review.xml', review_data)

print(response.text)

#	Post the read_at date *again* because Goodreads API is garbage
tree = ET.fromstring(response.text)
review_id = tree.find("id").text
review_data = {"review[read_at]":date[0:10]}
response = goodreads_api.post('https://www.goodreads.com/review/'+review_id+'.xml', review_data)
print(response.text)
